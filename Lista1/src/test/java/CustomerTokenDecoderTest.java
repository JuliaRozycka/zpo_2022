import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTokenDecoderTest {
    CustomerTokenDecoder customer;

    @BeforeAll
    static void init() {
        System.out.println("Let's begin tests!");
    }

    @BeforeEach
    void setup() {
        System.out.println("New test is about to begin.");
    }


    @Test
    @DisplayName("Matcher country code")
    void isCorrectCountryCode() {
        customer = new CustomerTokenDecoder();
        assertEquals(customer.getCountryCode("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1"),"PL", "Wrong country code.");
    }

    @Test
    @DisplayName("Matcher id")
    void isCorrectId() {
        customer = new CustomerTokenDecoder();
        assertEquals(customer.getId("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1"),4325, "Wrong country code.");
    }

    @Test
    @DisplayName("Matcher external id")
    void isCorrectExternalId() {
        customer = new CustomerTokenDecoder();
        assertEquals(customer.getExternalId("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1"),"fba110d0-4848-4dc0-9e8d-b1141cfdd64e", "Wrong external id.");
    }

    @Test
    @DisplayName("Matcher enabled service")
    void isCorrectService() {
        customer = new CustomerTokenDecoder();
        assertEquals(customer.getEnabledService("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1"),"12.43.54.6.2.1", "Wrong enabled services.");
    }


    @Test
    @DisplayName("Comparison of objects")
    void compareFridges() {
        Customer customer1 = new Customer();
        Customer customer2 = new Customer();
        assertNotSame(customer1, customer2, "References are equal.");
    }

    @Test
    @DisplayName("Correct format")
    void isCorrect() {
        customer = new CustomerTokenDecoder();
        assertTrue(customer.checkCustomerTokenFormat("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1"),"Wrong format.");
    }

    @Test
    @DisplayName("Incorrect format")
    void isNotCorrect() {
        customer = new CustomerTokenDecoder();
        assertFalse(customer.checkCustomerTokenFormat("PL4325fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1"),"Wrong format.");
    }

    @Test
    @DisplayName("DecodeException")
    void decideException(){
        customer = new CustomerTokenDecoder();
        assertThrows(RuntimeException.class, () -> customer.decode("PL4325fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1"));
    }

    @Test
    @DisplayName("Decode")
    void decode() {
        customer = new CustomerTokenDecoder();
        assertEquals(customer.decode("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1").toString(),"Customer{countryCode='PL', id=4325, externalId='fba110d0-4848-4dc0-9e8d-b1141cfdd64e', enabledServices='12.43.54.6.2.1'}","Wrong output.");
    }



    @Test
    @DisplayName("True")
    void isTrue() {
        customer = new CustomerTokenDecoder();
        assertTrue(customer instanceof CustomerTokenDecoder);
    }

    @AfterAll
    static void teardownAll() {
        System.out.println("Bye!");
    }

    @AfterEach
    void teardown() {
        System.out.println("End of test.");
    }

}