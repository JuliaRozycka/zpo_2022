/**
 * Keeps my service id - 12.
 */
public class ServiceConfig {

    private int serviceId = 12;

    public ServiceConfig() {
    }

    public int getServiceId() {
        return serviceId;
    }
}
