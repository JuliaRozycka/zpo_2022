import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class ServiceConfigTest {
    ServiceConfig serviceConfig;

    @BeforeAll
    static void init() {
        System.out.println("Let's begin tests!");
    }

    @BeforeEach
    void setup() {
        System.out.println("New test is about to begin.");
    }

    @Test
    @DisplayName("Null object")
    void idNull() {
        assertNull(serviceConfig,"Not null");
    }

    @Test
    @DisplayName("Not null")
    void notNUll() {
        serviceConfig = new ServiceConfig();
        assertNotNull(serviceConfig,"Null");
    }

    @Test
    @DisplayName("Id is 12")
    void isOurId(){
        serviceConfig = new ServiceConfig();
        assertEquals(serviceConfig.getServiceId(),12);
    }

    @Test
    @DisplayName("True")
    void isTrue() {
        serviceConfig = new ServiceConfig();
        assertTrue(serviceConfig instanceof ServiceConfig);
    }

    @Test
    @DisplayName("Comparison")
    void compare() {
        ServiceConfig serviceConfig1 = new ServiceConfig();
        ServiceConfig serviceConfig2 = new ServiceConfig();
        assertNotEquals(serviceConfig1,serviceConfig2,"They are equals.");
    }

    @Test
    @DisplayName("Ultimate validation")
    void validation() {
        serviceConfig = new ServiceConfig();
        int myId = serviceConfig.getServiceId();

        //check if customer has access
        CustomerTokenValidator customerTokenValidator = new CustomerTokenValidator();
        String[] ids = new String[]{String.valueOf(myId)};

        String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfIGZiYTExMGQwLTQ4NDgtNGRjMC05ZThkLWIxMTQxY2ZkZDY0ZTEyLjQzLjU0LjYuMi4xIiwibmFtZSI6IlBXciIsImlhdCI6MTUxNjIzOTAyMn0.ikK6v4yFhXnBf5M9ZYJi1cBVajEnwuu9dgYuWZYYE7g";
        assertEquals(customerTokenValidator.validateToken(ids, token).toString(),"Customer{countryCode='PL', id=4325, externalId='fba110d0-4848-4dc0-9e8d-b1141cfdd64e', enabledServices='12.43.54.6.2.1'}","Wrong output.");
    }

    @AfterAll
    static void teardownAll() {
        System.out.println("Bye!");
    }

    @AfterEach
    void teardown() {
        System.out.println("End of test.");
    }


}