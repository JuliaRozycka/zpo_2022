import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTokenValidatorTest {
    CustomerTokenValidator customer;

    @BeforeAll
    static void init() {
        System.out.println("Let's begin tests!");
    }

    @BeforeEach
    void setup() {
        System.out.println("New test is about to begin.");
    }


    @Test
    @DisplayName("Get customer token")
    void getCustomerToken() {
        customer = new CustomerTokenValidator();
        String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfIGZiYTExMGQwLTQ4NDgtNGRjMC05ZThkLWIxMTQxY2ZkZDY0ZTEyLjQzLjU0LjYuMi4xIiwibmFtZSI6IlBXciIsImlhdCI6MTUxNjIzOTAyMn0.ikK6v4yFhXnBf5M9ZYJi1cBVajEnwuu9dgYuWZYYE7g";
        assertEquals(customer.getCustomerToken(token),"PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1", "Wrong output.");
    }

    @Test
    @DisplayName("Exception customer token")
    void getCustomerTokenException() {
        customer = new CustomerTokenValidator();
        String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdVRva2VuIjoiUEw0MzI1ZXhfZmJhMTEwZDAtNDg0OC00ZGMwLTllOGQtYjExNDFjZmRkNjRlMTIuNDMuNTQuNi4yLjEiLCJuYW1lIjoiUFdyIiwiaWF0IjoxNTE2MjM5MDIyfQ==.ikK6v4yFhXnBf5M9ZYJi1cBVajEnwuu9dgYuWZYYE7g";
        assertThrows(RuntimeException.class, () -> customer.getCustomerToken(token),"Wrong output.");
    }

    @Test
    @DisplayName("Has access")
    void hasAccess() {
        customer = new CustomerTokenValidator();
        assertEquals(customer.validateToken(new String[]{"12"},"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfIGZiYTExMGQwLTQ4NDgtNGRjMC05ZThkLWIxMTQxY2ZkZDY0ZTEyLjQzLjU0LjYuMi4xIiwibmFtZSI6IlBXciIsImlhdCI6MTUxNjIzOTAyMn0.ikK6v4yFhXnBf5M9ZYJi1cBVajEnwuu9dgYuWZYYE7g").toString(),"Customer{countryCode='PL', id=4325, externalId='fba110d0-4848-4dc0-9e8d-b1141cfdd64e', enabledServices='12.43.54.6.2.1'}", "Wrong output.");
    }

    @Test
    @DisplayName("Has not access")
    void hasNotAccess() {
        customer = new CustomerTokenValidator();
        assertThrows(RuntimeException.class, () -> customer.validateToken(new String[]{"12"},"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfIGZiYTExMGQwLTQ4NDgtNGRjMC05ZThkLWIxMTQxY2ZkZDY0ZTE1LjQzLjU0LjYuMi4xIiwibmFtZSI6IlBXciIsImlhdCI6MTUxNjIzOTAyMn0=.ikK6v4yFhXnBf5M9ZYJi1cBVajEnwuu9dgYuWZYYE7g"), "Wrong output.");
    }

    @Test
    @DisplayName("True")
    void isTrue() {
        customer = new CustomerTokenValidator();
        assertTrue(customer instanceof CustomerTokenValidator);
    }

    @AfterAll
    static void teardownAll() {
        System.out.println("Bye!");
    }

    @AfterEach
    void teardown() {
        System.out.println("End of test.");
    }

}