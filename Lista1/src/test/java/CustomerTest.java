import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTest {
    Customer customer;

    @BeforeAll
    static void init() {
        System.out.println("Let's begin tests!");
    }

    @BeforeEach
    void setup() {
        System.out.println("New test is about to begin.");
    }


    @Test
    @DisplayName("Null id")
    void isNullId() {
        customer = new Customer();
        assertNull(customer.getId(),"Customer id is not null");
   }

    @Test
    @DisplayName("Null country code")
    void isNullCountryCode() {
        customer = new Customer();
        assertNull(customer.getCountryCode(),"Customer country code is not null");
    }

    @Test
    @DisplayName("Null external id")
    void isNullExternalId() {
        customer = new Customer();
        assertNull(customer.getExternalId(),"External id is not null");
    }

    @Test
    @DisplayName("Null enabled services")
    void isNullEnabledServices() {
        customer = new Customer();
        assertNull(customer.getEnabledServices(),"Enabled services are not null");
    }

    @Test
    @DisplayName("Getters")
    void getters() {
        customer = new Customer("PL", 1234L,"fba110d0-4848-4dc0-9e8d-b1141cfdd64e","12.3.4.5.6");
        assertEquals(customer.getId(),1234,"Wrong id.");
        assertEquals(customer.getCountryCode(),"PL","Wrong country code.");
        assertEquals(customer.getExternalId(),"fba110d0-4848-4dc0-9e8d-b1141cfdd64e","Wrong external id.");
        assertEquals(customer.getEnabledServices(),"12.3.4.5.6","Wrong enabled service.");
    }

    @Test
    @DisplayName("Setters")
    void setters() {
        customer = new Customer();
        customer.setCountryCode("PL");
        customer.setId(1234L);
        customer.setExternalId("fba110d0-4848-4dc0-9e8d-b1141cfdd64e");
        customer.setEnabledServices("12.3.4.5.6");
        assertEquals(customer.getId(),1234,"Wrong id.");
        assertEquals(customer.getCountryCode(),"PL","Wrong country code.");
        assertEquals(customer.getExternalId(),"fba110d0-4848-4dc0-9e8d-b1141cfdd64e","Wrong external id.");
        assertEquals(customer.getEnabledServices(),"12.3.4.5.6","Wrong enabled service.");
    }

    @Test
    @DisplayName("toString")
    void toStringTest() {
        customer = new Customer("PL", 1234L,"fba110d0-4848-4dc0-9e8d-b1141cfdd64e","12.3.4.5.6");
        assertEquals(customer.toString(),"Customer{countryCode='PL', id=1234, externalId='fba110d0-4848-4dc0-9e8d-b1141cfdd64e', enabledServices='12.3.4.5.6'}","Wrong output.");
    }

    @Test
    @DisplayName("Comparison of objects")
    void compareFridges() {
        Customer customer1 = new Customer();
        Customer customer2 = new Customer();
        assertNotSame(customer1, customer2, "References are equal.");
    }

    @Test
    @DisplayName("True")
    void isTrue() {
        customer = new Customer();
        assertTrue(customer instanceof Customer);
    }

    @AfterAll
    static void teardownAll() {
        System.out.println("Bye!");
    }

    @AfterEach
    void teardown() {
        System.out.println("End of test.");
    }

    @Test
    void getCountryCode() {
        customer = new Customer("PL", 1234L,"fba110d0-4848-4dc0-9e8d-b1141cfdd64e","12.3.4.5.6");
        assertEquals("PL", customer.getCountryCode(), "Wrong id.");
    }

    @Test
    void setCountryCode() {
        customer = new Customer();
        customer.setCountryCode("PL");
        assertEquals("PL", customer.getCountryCode(), "Wrong id.");
    }

    @Test
    void getId() {
        customer = new Customer("PL", 1234L,"fba110d0-4848-4dc0-9e8d-b1141cfdd64e","12.3.4.5.6");
        assertEquals(1234, customer.getId(), "Wrong id.");
    }

    @Test
    void setId() {
        customer = new Customer();
        customer.setId(1234L);
        assertEquals(1234, customer.getId(), "Wrong id.");
    }

    @Test
    void getExternalId() {
        customer = new Customer("PL", 1234L,"fba110d0-4848-4dc0-9e8d-b1141cfdd64e","12.3.4.5.6");
        assertEquals("fba110d0-4848-4dc0-9e8d-b1141cfdd64e", customer.getExternalId(), "Wrong id.");
    }

    @Test
    void setExternalId() {
        customer = new Customer();
        customer.setExternalId("fba110d0-4848-4dc0-9e8d-b1141cfdd64e");
        assertEquals("fba110d0-4848-4dc0-9e8d-b1141cfdd64e", customer.getExternalId(), "Wrong id.");
    }

    @Test
    void getEnabledServices() {
        customer = new Customer("PL", 1234L,"fba110d0-4848-4dc0-9e8d-b1141cfdd64e","12.3.4.5.6");
        assertEquals("12.3.4.5.6", customer.getEnabledServices(), "Wrong id.");
    }

    @Test
    void setEnabledServices() {
        customer = new Customer();
        customer.setEnabledServices("12.3.4.5.6");
        assertEquals("12.3.4.5.6", customer.getEnabledServices(), "Wrong id.");
    }
}