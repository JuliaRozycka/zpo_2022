import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Auxiliary class. Has a method - decode(String token)
 * that creates and returns Customer class object based on token.
 */
public class CustomerTokenDecoder {

    public String getCountryCode(String customerToken) {
        //two capital letters at the beginning
        String patternString = "^[A-Z]{2}";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(customerToken);
        String countryCode = "";
        if (matcher.find())
            countryCode = matcher.group();

        return countryCode;
    }

    public Long getId(String customerToken) {
        //random number of digits
        String patternString = "[A-Z]{2}[0-9]+ex_";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(customerToken);
        String matchedString = "";
        if (matcher.find())
            matchedString = matcher.group();
        String id = matchedString.substring(2).replace("ex_", "");

        return Long.valueOf(id);
    }

    public String getExternalId(String customerToken) {
        //GUID format
        String patternString = "[{]?[0-9a-fA-F]{8}-([0-9a-fA-F]{4}-){3}[0-9a-fA-F]{12}[}]?";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(customerToken);
        String externalId = "";
        if (matcher.find())
            externalId = matcher.group();

        return externalId;
    }

    public String getEnabledService(String customerToken) {
        //numbers separated by a comma
        String patternString = "([0-9]+(\\.[0-9]+)+)$";

        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(customerToken);
        String enabledService = "";
        if (matcher.find())
            enabledService = matcher.group();

        return enabledService;
    }

    public Customer decode(String customerToken) throws RuntimeException {

        if (!checkCustomerTokenFormat(customerToken)) {
            throw new RuntimeException("Incorrect body.");
        }

        String countryCode = getCountryCode(customerToken);
        Long id = getId(customerToken);
        String externalId = getExternalId(customerToken);
        String enabledService = getEnabledService(customerToken);

        return new Customer(countryCode, id, externalId, enabledService);
    }

    public boolean checkCustomerTokenFormat(String customerToken) {
        String patternString = "[A-Z]{2}[0-9]+ex_[{]?[0-9a-fA-F]{8}-([0-9a-fA-F]{4}-){3}[0-9a-fA-F]{12}[}]?([0-9]+(\\.[0-9]+)+)$";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(customerToken.replaceAll(" ", ""));
        return matcher.find();
    }

}
