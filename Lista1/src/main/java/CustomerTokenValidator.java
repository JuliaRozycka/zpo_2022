import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Arrays;
import java.util.Base64;

/**
 * Class that gets id (or many ids) and customer token.
 * It validates the whole token, creates Customer class object and checks if
 * given customer has access to service.
 * Returns Customer class object.
 */
public class CustomerTokenValidator {

    public Customer validateToken(String[] ids, String token) throws RuntimeException {
        CustomerTokenDecoder customerTokenDecoder = new CustomerTokenDecoder();
        String customerToken = getCustomerToken(token);
        Customer customer = customerTokenDecoder.decode(customerToken);


        if (Arrays.stream(ids).noneMatch(customerTokenDecoder.getEnabledService(customerToken)::contains)) {
            throw new RuntimeException("Customer doesn't have access for this service.");
        }

        return customer;
    }

    public String getCustomerToken(String token) throws RuntimeException {
        String[] list = token.split("\\.");
        byte[] bytes = Base64.getDecoder().decode(list[1]);
        String decodedString = new String(bytes);
        JsonObject jsonObject = new JsonParser().parse(decodedString).getAsJsonObject();
        if(!jsonObject.has("customerToken")){
            throw new RuntimeException("The token doesn't have customer token.");
        }
        return jsonObject.get("customerToken").getAsString();
    }

}
