/**
 * This class has all the fields that can be
 * separated from the token: countryCode, id, externalId and enabledServices.
 */
public class Customer {
    private String countryCode;
    private Long id;
    private String externalId;
    private String enabledServices;

    public Customer(String countryCode, Long id, String externalId, String enabledServices) {
        this.countryCode = countryCode;
        this.id = id;
        this.externalId = externalId;
        this.enabledServices = enabledServices;
    }

    public Customer() {
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getEnabledServices() {
        return enabledServices;
    }

    public void setEnabledServices(String enabledServices) {
        this.enabledServices = enabledServices;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "countryCode='" + countryCode + '\'' +
                ", id=" + id +
                ", externalId='" + externalId + '\'' +
                ", enabledServices='" + enabledServices + '\'' +
                '}';
    }
}
